<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160815005917 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE trainer (id INT AUTO_INCREMENT NOT NULL, category_id SMALLINT NOT NULL, lastname VARCHAR(30) NOT NULL, employment_date DATE NOT NULL, created DATETIME NOT NULL, updated DATETIME NOT NULL, INDEX IDX_C515082012469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dog (id INT AUTO_INCREMENT NOT NULL, breed_id SMALLINT NOT NULL, trainer_id INT DEFAULT NULL, name VARCHAR(10) NOT NULL, has_pedigree TINYINT(1) NOT NULL, birthday DATE NOT NULL, created DATETIME NOT NULL, updated DATETIME NOT NULL, INDEX IDX_812C397DA8B4A30F (breed_id), INDEX IDX_812C397DFB08EDF6 (trainer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dog_breed (id SMALLINT AUTO_INCREMENT NOT NULL, breed VARCHAR(20) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trainer_category (id SMALLINT NOT NULL, category VARCHAR(20) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE trainer ADD CONSTRAINT FK_C515082012469DE2 FOREIGN KEY (category_id) REFERENCES trainer_category (id)');
        $this->addSql('ALTER TABLE dog ADD CONSTRAINT FK_812C397DA8B4A30F FOREIGN KEY (breed_id) REFERENCES dog_breed (id)');
        $this->addSql('ALTER TABLE dog ADD CONSTRAINT FK_812C397DFB08EDF6 FOREIGN KEY (trainer_id) REFERENCES trainer (id) ON DELETE SET NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE dog DROP FOREIGN KEY FK_812C397DFB08EDF6');
        $this->addSql('ALTER TABLE dog DROP FOREIGN KEY FK_812C397DA8B4A30F');
        $this->addSql('ALTER TABLE trainer DROP FOREIGN KEY FK_C515082012469DE2');
        $this->addSql('DROP TABLE trainer');
        $this->addSql('DROP TABLE dog');
        $this->addSql('DROP TABLE dog_breed');
        $this->addSql('DROP TABLE trainer_category');
    }
}
