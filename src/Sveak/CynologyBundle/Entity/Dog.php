<?php

namespace Sveak\CynologyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Sveak\CynologyBundle\Entity\Repository\DogRepository")
 * @ORM\Table(name="dog")
 * @ORM\HasLifecycleCallbacks()
 */
class Dog
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="DogBreed", inversedBy="dogs")
     * @ORM\JoinColumn(name="breed_id", referencedColumnName="id", nullable=false)
     */
    protected $breed;

    /**
     * @var Trainer
     *
     * @ORM\ManyToOne(targetEntity="Trainer", inversedBy="dogs")
     * @ORM\JoinColumn(name="trainer_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $trainer;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $has_pedigree;

    /**
     * @ORM\Column(type="date")
     */
    protected $birthday;

    /**
     * @var string
     */
    protected $age;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    public function __construct()
    {
        $this->setCreated(new \DateTime());
        $this->setUpdated(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Dog
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get hasPedigree
     *
     * @return boolean
     */
    public function getHasPedigree()
    {
        return $this->has_pedigree;
    }

    /**
     * Set hasPedigree
     *
     * @param boolean $hasPedigree
     *
     * @return Dog
     */
    public function setHasPedigree($hasPedigree)
    {
        $this->has_pedigree = $hasPedigree;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Dog
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Dog
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedValue()
    {
        $this->setUpdated(new \DateTime());
    }

    /**
     * Get breed
     *
     * @return \Sveak\CynologyBundle\Entity\DogBreed
     */
    public function getBreed()
    {
        return $this->breed;
    }

    /**
     * Set breed
     *
     * @param DogBreed $breed
     *
     * @return Dog
     */
    public function setBreed(DogBreed $breed = null)
    {
        $this->breed = $breed;

        return $this;
    }

    /**
     * Get trainer
     *
     * @return \Sveak\CynologyBundle\Entity\Trainer
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * Set trainer
     *
     * @param Trainer $trainer
     *
     * @return Dog
     */
    public function setTrainer(Trainer $trainer = null)
    {
        $this->trainer = $trainer;

        return $this;
    }

    public function getTrainerName()
    {
        if (false === is_null($this->trainer))
            return $this->trainer->getLastname();
        else
            return 'No Trainer';
    }

    /**
     * @return string
     */
    public function getAge()
    {
        $ageTime = (time() - $this->getBirthday()->getTimestamp()) / 86400;

        if ($ageTime < 30) {
            $age = 'less than 1 months';
        } elseif ($ageTime <= 365) {
            $age = floor($ageTime / 30) . ' months';
        } else {
            $age = floor($ageTime / 365) . ' years';
        }

        return $age;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return Dog
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }
}
