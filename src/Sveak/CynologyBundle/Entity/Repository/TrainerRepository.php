<?php

namespace Sveak\CynologyBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class TrainerRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findAll()
    {
        return $query = $this->createQueryBuilder('t')
            ->select('t, c, d')
            ->innerJoin('t.category', 'c')
            ->leftJoin('t.dogs', 'd')
            ->orderBy('t.id');
    }
}
