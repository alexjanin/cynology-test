<?php

namespace Sveak\CynologyBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class DogRepository extends EntityRepository
{
    public function findAll()
    {
        return $this->createQueryBuilder('d')
            ->select('d, b, t')
            ->innerJoin('d.breed', 'b')
            ->leftJoin('d.trainer', 't')
            ->orderBy('d.id');
    }
}
