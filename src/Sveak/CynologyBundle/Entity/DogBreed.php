<?php

namespace Sveak\CynologyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Sveak\CynologyBundle\Entity\Repository\DogBreedRepository")
 * @ORM\Table(name="dog_breed")
 */
class DogBreed
{
    /**
     * @ORM\Id
     * @ORM\Column(type="smallint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $breed;

    /**
     * @ORM\OneToMany(targetEntity="Sveak\CynologyBundle\Entity\Dog", mappedBy="breed")
     */
    protected $dogs;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dogs = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add dog
     *
     * @param Dog $dog
     *
     * @return DogBreed
     */
    public function addDog(Dog $dog)
    {
        $this->dogs[] = $dog;

        return $this;
    }

    /**
     * Remove dog
     *
     * @param Dog $dog
     */
    public function removeDog(Dog $dog)
    {
        $this->dogs->removeElement($dog);
    }

    /**
     * Get dogs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDogs()
    {
        return $this->dogs;
    }

    function __toString()
    {
        return $this->getBreed();
    }

    /**
     * Get breed
     *
     * @return string
     */
    public function getBreed()
    {
        return $this->breed;
    }

    /**
     * Set breed
     *
     * @param string $breed
     *
     * @return DogBreed
     */
    public function setBreed($breed)
    {
        $this->breed = $breed;

        return $this;
    }
}
