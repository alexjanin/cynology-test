<?php

namespace Sveak\CynologyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Sveak\CynologyBundle\Entity\Repository\TrainerCategoryRepository")
 * @ORM\Table(name="trainer_category")
 */
class TrainerCategory
{
    const HIGHEST_CATEGORY = 0;
    const FIRST_CATEGORY = 1;
    const SECOND_CATEGORY = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="smallint")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $category;

    /**
     * @ORM\OneToMany(targetEntity="Sveak\CynologyBundle\Entity\Trainer", mappedBy="category")
     */
    protected $trainers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->trainers = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return TrainerCategory
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Add trainer
     *
     * @param Trainer $trainer
     *
     * @return TrainerCategory
     */
    public function addTrainer(Trainer $trainer)
    {
        $this->trainers[] = $trainer;

        return $this;
    }

    /**
     * Remove trainer
     *
     * @param Trainer $trainer
     */
    public function removeTrainer(Trainer $trainer)
    {
        $this->trainers->removeElement($trainer);
    }

    /**
     * Get trainers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrainers()
    {
        return $this->trainers;
    }

    function __toString()
    {
        return $this->getCategory();
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set category
     *
     * @param string $category
     * @return TrainerCategory
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }
}
