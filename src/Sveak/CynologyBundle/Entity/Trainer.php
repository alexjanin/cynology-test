<?php

namespace Sveak\CynologyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Sveak\CynologyBundle\Entity\Repository\TrainerRepository")
 * @ORM\Table(name="trainer")
 * @ORM\HasLifecycleCallbacks()
 */
class Trainer
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    protected $lastname;

    /**
     * @ORM\Column(type="date")
     */
    protected $employment_date;

    /**
     * @ORM\ManyToOne(targetEntity="TrainerCategory", inversedBy="trainers")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
     */
    protected $category;

    /**
     * @ORM\OneToMany(targetEntity="Dog", mappedBy="trainer")
     */
    protected $dogs;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    /**
     * @var string
     */
    protected $experience;

    public function __construct()
    {
        $this->dogs = new ArrayCollection();

        $this->setCreated(new \DateTime());
        $this->setUpdated(new \DateTime());
    }

    /**
     * @return string
     */
    public function getExperience()
    {
        $expTime = (time() - $this->getEmploymentDate()->getTimestamp()) / 86400;

        if ($expTime < 1) {
            $experience = 'less than 1 day';
        } elseif ($expTime <= 30) {
            $experience = floor($expTime) . ' days';
        } elseif ($expTime <= 365) {
            $experience = floor($expTime / 30) . ' months';
        } else {
            $experience = floor($expTime / 365) . ' years';
        }

        return $experience;
    }

    /**
     * Get employment_date
     *
     * @return \DateTime
     */
    public function getEmploymentDate()
    {
        return $this->employment_date;
    }

    /**
     * Set employment_date
     *
     * @param \DateTime $employmentDate
     * @return Trainer
     */
    public function setEmploymentDate($employmentDate)
    {
        $this->employment_date = $employmentDate;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Trainer
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Trainer
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedValue()
    {
        $this->setUpdated(new \DateTime());
    }

    /**
     * Get category
     *
     * @return TrainerCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set category
     *
     * @param TrainerCategory $category
     * @return Trainer
     */
    public function setCategory(TrainerCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Add dog
     *
     * @param Dog $dog
     *
     * @return Trainer
     */
    public function addDog(Dog $dog)
    {
        $this->dogs[] = $dog;

        return $this;
    }

    /**
     * Remove dog
     *
     * @param Dog $dog
     */
    public function removeDog(Dog $dog)
    {
        $this->dogs->removeElement($dog);
    }

    /**
     * Get dogs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDogs()
    {
        return $this->dogs;
    }

    /**
     * @return string
     */
    function __toString()
    {
        if (false === is_null($this->getLastname()))
            return $this->getLastname();
        else
            return '';
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Trainer
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }
}
