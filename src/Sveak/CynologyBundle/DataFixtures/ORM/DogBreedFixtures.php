<?php

namespace Sveak\CynologyBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sveak\CynologyBundle\Entity\DogBreed;

class DogBreedFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $boxer = new DogBreed();
        $boxer->setBreed('Boxer');
        $manager->persist($boxer);

        $bullterrier = new DogBreed();
        $bullterrier->setBreed('Bullterrier');
        $manager->persist($bullterrier);

        $dalmatian = new DogBreed();
        $dalmatian->setBreed('Dalmatian');
        $manager->persist($dalmatian);

        $doberman = new DogBreed();
        $doberman->setBreed('Doberman');
        $manager->persist($doberman);

        $labrador = new DogBreed();
        $labrador->setBreed('Labrador');
        $manager->persist($labrador);

        $german = new DogBreed();
        $german->setBreed('German Shepherd');
        $manager->persist($german);

        $manager->flush();

        $this->addReference('breed-0', $boxer);
        $this->addReference('breed-1', $bullterrier);
        $this->addReference('breed-2', $dalmatian);
        $this->addReference('breed-3', $doberman);
        $this->addReference('breed-4', $labrador);
        $this->addReference('breed-5', $german);
    }

    public function getOrder()
    {
        return 30;
    }
}