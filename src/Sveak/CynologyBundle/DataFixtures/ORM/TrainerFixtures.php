<?php

namespace Sveak\CynologyBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sveak\CynologyBundle\Entity\Trainer;

class TrainerFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 20; $i++) {
            $trainer = new Trainer();
            $trainer->setLastname('Trainer' . $i);
            $trainer->setCategory($manager->merge($this->getReference('category-' . ($i % 3))));
            $trainer->setEmploymentDate(new \DateTime(date('Y-m-d', strtotime('-' . ($i * 25) . ' days'))));
            $manager->persist($trainer);

            $this->addReference('trainer-' . $i, $trainer);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 20;
    }
}