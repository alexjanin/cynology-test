<?php

namespace Sveak\CynologyBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sveak\CynologyBundle\Entity\TrainerCategory;

class TrainerCategoryFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $highest = new TrainerCategory();
        $highest->setId(0);
        $highest->setCategory('Highest');
        $manager->persist($highest);

        $first = new TrainerCategory();
        $first->setId(1);
        $first->setCategory('First');
        $manager->persist($first);

        $second = new TrainerCategory();
        $second->setId(2);
        $second->setCategory('Second');
        $manager->persist($second);

        $manager->flush();

        $this->addReference('category-0', $highest);
        $this->addReference('category-1', $first);
        $this->addReference('category-2', $second);
    }

    public function getOrder()
    {
        return 10;
    }
}