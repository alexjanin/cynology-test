<?php

namespace Sveak\CynologyBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sveak\CynologyBundle\Entity\Dog;

class DogFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 40; $i++) {
            $dog = new Dog();
            $dog->setName('Dog' . $i);
            $dog->setBreed($manager->merge($this->getReference('breed-' . ($i % 6))));
            $dog->setTrainer($manager->merge($this->getReference('trainer-' . ceil($i / 2))));
            $dog->setHasPedigree($i % 2 ? true : false);
            $dog->setBirthday(new \DateTime(date('Y-m-d', strtotime('-' . ($i * 25) . ' days'))));
            $manager->persist($dog);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 40;
    }
}