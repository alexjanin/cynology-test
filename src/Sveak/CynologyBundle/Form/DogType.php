<?php

namespace Sveak\CynologyBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DogType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('has_pedigree', CheckboxType::class, array(
                'required' => false
            ))
            ->add('birthday', DateType::class)
            ->add('trainer', EntityType::class, array(
                'class' => 'Sveak\CynologyBundle\Entity\Trainer',
                'placeholder' => 'No Trainer',
                'required' => false
            ))
            ->add('breed');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sveak\CynologyBundle\Entity\Dog'
        ));
    }
}
