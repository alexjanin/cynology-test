<?php

namespace Sveak\CynologyBundle\Controller;

use Sveak\CynologyBundle\Entity\Feedback;
use Sveak\CynologyBundle\Form\FeedbackType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PageController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('SveakCynologyBundle:Page:index.html.twig');
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function feedbackAction(Request $request)
    {
        $feedback = new Feedback();

        $form = $this->createForm(FeedbackType::class, $feedback);

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $message = \Swift_Message::newInstance()
                    ->setSubject($feedback->getSubject())
                    ->setFrom('sender@mss22.ru')
                    ->addReplyTo($feedback->getEmail())
                    ->setTo($this->container->getParameter('sveak_cynology.emails.contact_email'))
                    ->setBody($this->renderView('SveakCynologyBundle:Page:feedbackEmail.txt.twig',
                        array('feedback' => $feedback)));

                $this->get('mailer')->send($message);

                $this->get('session')->getFlashBag()
                    ->add('cynology-notice', 'Your message was successfully sent. Thank you!');

                return $this->redirect($this->generateUrl('sveak_cynology_feedback'));
            }
        }

        return $this->render('SveakCynologyBundle:Page:feedback.html.twig', array(
            'form' => $form->createView()
        ));
    }
}