<?php

namespace Sveak\CynologyBundle\Controller;

use Sveak\CynologyBundle\Entity\Dog;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sveak\CynologyBundle\Entity\Trainer;
use Sveak\CynologyBundle\Form\TrainerType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Trainer controller.
 *
 */
class TrainerController extends Controller
{
    /**
     * Lists all Trainer entities.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $trainers = $this->getDoctrine()
            ->getManager()
            ->getRepository('SveakCynologyBundle:Trainer')->findAll();

        // Creating pagnination
        $paginator = $this->container->get('knp_paginator');
        $pagination = $paginator->paginate(
            $trainers,
            $this->container->get('request')->query->get('page', 1),
            $this->container->getParameter('sveak_cynology.trainer.page.limit')
        );

        $form_builder = $this->createFormBuilder()->setMethod('DELETE');

        return $this->render('SveakCynologyBundle:Trainer:index.html.twig',
            compact(
                'pagination',
                'form_builder'
            )
        );
    }

    /**
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $trainer = new Trainer();
        $form = $this->createForm('Sveak\CynologyBundle\Form\TrainerType', $trainer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($trainer);
            $em->flush();

            return $this->redirectToRoute('sveak_cynology_trainer_index');
        }

        return $this->render('SveakCynologyBundle:Trainer:new.html.twig', array(
            'trainer' => $trainer,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Trainer entity.
     *
     * @param Request $request
     * @param Trainer $trainer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Trainer $trainer)
    {
        $deleteForm = $this->createDeleteForm($trainer);
        $editForm = $this->createForm('Sveak\CynologyBundle\Form\TrainerType', $trainer);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($trainer);
            $em->flush();

            return $this->redirectToRoute('sveak_cynology_trainer_edit', array('id' => $trainer->getId()));
        }

        return $this->render('SveakCynologyBundle:Trainer:edit.html.twig', array(
            'trainer'      => $trainer,
            'edit_form'    => $editForm->createView(),
            'delete_form'  => $deleteForm->createView(),
            'form_builder' => $this->createFormBuilder()->setMethod('DELETE'),
        ));
    }

    /**
     * Creates a form to delete a Trainer entity.
     *
     * @param Trainer $trainer The Trainer entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Trainer $trainer = null)
    {
        $formBuilder = $this->createFormBuilder();

        if (false === is_null($trainer))
            $formBuilder->setAction($this->generateUrl('sveak_cynology_trainer_delete', array('id' => $trainer->getId())));

        return $formBuilder->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Deletes a Trainer entity.
     *
     * @param Request $request
     * @param Trainer $trainer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Trainer $trainer)
    {
        $form = $this->createDeleteForm($trainer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($trainer);
            $em->flush();
        }

        return $this->redirectToRoute('sveak_cynology_trainer_index');
    }

    /**
     * @param Request $request
     * @param Trainer $trainer
     * @param Dog $dog
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function looseAction(Request $request, Trainer $trainer, Dog $dog)
    {
        $form = $this->createLooseForm($trainer, $dog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dog->setTrainer(null);
            $em = $this->getDoctrine()->getManager();
            $em->persist($dog);
            $em->flush();
        }

        return $this->redirectToRoute('sveak_cynology_trainer_edit', array('id' => $trainer->getId()));
    }

    /**
     * @param Trainer|null $trainer
     * @param Dog|null $dog
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createLooseForm(Trainer $trainer, Dog $dog = null)
    {
        $formBuilder = $this->createFormBuilder();

        if (false === is_null($dog))
            $formBuilder->setAction($this->generateUrl('sveak_cynology_trainer_loose_dog', array(
                'id'  => $trainer->getId(),
                'dog' => $dog->getId(),
            )));

        return $formBuilder->setMethod('DELETE')
            ->getForm();
    }
}
