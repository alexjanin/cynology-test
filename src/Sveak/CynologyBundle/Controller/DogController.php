<?php

namespace Sveak\CynologyBundle\Controller;


use Sveak\CynologyBundle\Entity\Dog;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DogController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $dogs = $this->getDoctrine()
            ->getManager()
            ->getRepository('SveakCynologyBundle:Dog')->findAll();

        // Creating pagnination
        $paginator = $this->container->get('knp_paginator');
        $pagination = $paginator->paginate(
            $dogs,
            $this->container->get('request')->query->get('page', 1),
            $this->container->getParameter('sveak_cynology.dog.page.limit')
        );

        $form_builder = $this->createFormBuilder()->setMethod('DELETE');

        return $this->render('SveakCynologyBundle:Dog:index.html.twig', compact(
            'pagination',
            'form_builder'
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $dog = new Dog();
        $form = $this->createForm('Sveak\CynologyBundle\Form\DogType', $dog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($dog);
            $em->flush();

            return $this->redirectToRoute('sveak_cynology_dog_index');
        }

        return $this->render('SveakCynologyBundle:Dog:new.html.twig', array(
            'dog' => $dog,
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Request $request
     * @param Dog $dog
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Dog $dog)
    {
        $deleteForm = $this->createDeleteForm($dog);
        $editForm = $this->createForm('Sveak\CynologyBundle\Form\DogType', $dog);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($dog);
            $em->flush();

            return $this->redirectToRoute('sveak_cynology_dog_edit', array('id' => $dog->getId()));
        }

        return $this->render('SveakCynologyBundle:Dog:edit.html.twig', array(
            'dog' => $dog,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @param Dog|null $dog
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Dog $dog = null)
    {
        $formBuilder = $this->createFormBuilder();

        if (false === is_null($dog))
            $formBuilder->setAction($this->generateUrl('sveak_cynology_dog_delete', array('id' => $dog->getId())));

        return $formBuilder->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @param Request $request
     * @param Dog $dog
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Dog $dog)
    {
        $form = $this->createDeleteForm($dog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($dog);
            $em->flush();
        }

        return $this->redirectToRoute('sveak_cynology_dog_index');
    }
}