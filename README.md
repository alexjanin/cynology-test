# Cynology

## Installation:

Pretty simple with [Composer](http://packagist.org), run:

```sh
composer install
```

## Create database

```sh
php app/console doctrine:database:create
```

## Apply migration

```sh
php app/console doctrine:migrations:migrate
```

## Load DataFixtures

```sh
php app/console doctrine:fixtures:load
```